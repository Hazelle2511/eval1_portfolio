

# Porfolio using  HTML, vanilla CSS, and vanilla JS

# Contenu
    Projects Page
      * Présentation de vos projets (avec un lien vers un dépôt gitlab et, si possible, une version en ligne)
     

    Blog Page
        *  En bonus, vous pouvez proposer une partie blog.


    CV Page
       * Présentation de votre parcours
       * Présentation de vos compétences et des technologies que vous connaissez
    
    Contact Page 
         * Vos coordonnées ainsi qu'un formulaire de contact (qui fonctionne).
         * I will automatically received the messages you've sent 

 # Critères pour la revue de code
   
    * Le portfolio est responsive.              OUI
    * L'ecoindex est A ou B.                      A
    * Le Performance Score est au moins de 80%.   89.6%
    *  Le site ne comporte aucune erreur d'accessibilité. C'est tout bon
    * Le code est propre est indenté. Tous les critères de performance des compétences C1, C2 et C3 présentées dans le référentiel. C'est tout bon

# Site en ligne
    https://hazelle2511.gitlab.io/eval1_portfolio/


